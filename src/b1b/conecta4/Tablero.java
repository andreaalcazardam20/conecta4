package b1b.conecta4;

public class Tablero {

    // Atributos
    private int alto, ancho;
    private Estado[][] casillas;
    private Estado ganador = null;
    private Estado turno = Estado.JUGADOR1;
    private int numeroJugadas = 0;

    // Constructores

    /**
     * Crea un Tablero por defecto (8x8) y llama al m&eacute;todo
     * {@link b1b.conecta4.Tablero#limpiarTablero(Estado[][]) limpiarTablero}
     */

    public Tablero() {

        casillas = new Estado[8][8];
        limpiarTablero(casillas);

    }

    /**
     * Crea un Tablero con las dimensiones especificadas y
     * llama al m&eacute;todo {@link b1b.conecta4.Tablero#limpiarTablero(Estado[][]) limpiarTablero}.
     * Si cualquiera de las dimensiones introducidas son menor a 5
     * se estableceran automaticamente a 5 para respetar el tama&ntilde;o
     * m&iacute;nimo del tablero.
     *
     * @param alto  Altura del tablero.
     * @param ancho Anchura del tablero
     */

    public Tablero(int alto, int ancho) {

        int altura, anchura;

        if (alto < 5)
            altura = 5;
        else
            altura = alto;

        if (ancho < 5)
            anchura = 5;
        else
            anchura = ancho;

        casillas = new Estado[anchura][altura];
        limpiarTablero(casillas);

    }

    /**
     * Llena un tablero ya instanciado con Estado.VACIO.
     *
     * @param tablero Tablero a llenar
     */

    private void limpiarTablero(Estado[][] tablero) {
        for (int i = 0; i < tablero.length; i++) {
            for (int j = 0; j < tablero[i].length; j++) {
                casillas[i][j] = Estado.VACIO;
            }
        }
    }

    /**
     * Devuelve si ha finalizado el juego comprobando si hay
     * alg&uacute;n ganador
     *
     * @return true si el juego ha finalizado
     */

    private boolean juegoFinalizado() {
        if (this.ganador == null)
            return false;
        else
            return true;
    }

    /**
     * Inicia el juego {@link b1b.conecta4.Tablero#limpiarTablero(Estado[][]) limpiando el tablero}
     * y estableciendo el siguiente turno para el JUGADOR1.
     */

    public void iniciarJuego() {

        limpiarTablero(casillas);
        turno = Estado.JUGADOR1;

    }

    /**
     * Retorna el valor de la casilla especificada.
     *
     * @param x Posicion horizontal de la casilla.
     * @param y Posicion vertical de la casilla.
     * @return Valor de la casilla.
     */

    public Estado getEstadoCasillaTablero(int x, int y) {
        return casillas[x][y];
        //TODO A&ntilde;adir error si la casilla no existe.
    }

    /**
     * Coloca una ficha del jugador activo en la posici&oacute;n indicada
     * y comprueba si el juego ha finalizado.
     *
     * @param x Posicion horizontal de la casilla.
     * @param y Posicion vertical de la casilla.
     * @return true si el juego ha finalizado.
     */

    public boolean hacerJugada(int x, int y) {

        casillas[x][y] = turno;

        if (turno == Estado.JUGADOR1)
            turno = Estado.JUGADOR2;
        else
            turno = Estado.JUGADOR1;

        return false;
        //TODO A&ntilde;adir error si la casilla no existe.
    }
}