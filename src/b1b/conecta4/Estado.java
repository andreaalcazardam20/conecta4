package b1b.conecta4;

/**
 * Posibles estados de una casilla.
 * Tambi&eacute;n utilizado para definir el jugador activo en el turno.
 */

public enum Estado {
    VACIO, JUGADOR1, JUGADOR2
}
